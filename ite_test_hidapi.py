import hid
import time

VENDOR_ID = 0x048d # ITE Devices
PRODUCT_ID = 0x8910 # IT821x

device = hid.device()
device.open(VENDOR_ID, PRODUCT_ID)
print(f'Connected to IT821x ({hex(VENDOR_ID)}:{hex(PRODUCT_ID)})\n')

# Define the size of our keyboard 'grid'
KB_COLS = 20
KB_ROWS = 6

# backlight brightness stuff
MAX_BRIGHTNESS = [0xCC, 0x09, 0x04, 0x00, 0x00, 0x00, 0x00]
MIN_BRIGHTNESS = [0xCC, 0x09, 0x00, 0x00, 0x00, 0x00, 0x0A]

# Set brightness to max so we can see things!
device.send_feature_report(MAX_BRIGHTNESS)

def get_led_id(row, col):
    """Get the ID of LED at row, col"""
    return (row & 0x07) << 5 | (col & 0x1f)

GENTOO_KEYS = [(3,6), (2,4), (4,8), (2,6), (2,10)]

def do_gentoo_keys():
    """Set 'Gentoo' keys Purple"""
    for key in GENTOO_KEYS:
        device.send_feature_report([0xCC, 0x01, get_led_id(key[0],key[1]), 0xFF, 0x00, 0xFF, 0x6C])

# Set all keys to white
def white_keys():
    """Set all keys on keyboard to white"""
    c = 0
    while c < KB_COLS:
        r = 0
        while r < KB_ROWS:
            device.send_feature_report([0xCC, 0x01, get_led_id(r,c), 0xFF, 0xFF, 0xFF, 0x6C])
            r += 1
        c += 1

def set_keys_to_colour(colour):
    """Set all keys on the keyboard to requested hex colour"""
    red = int(colour[:2], 16)
    green = int(colour[2:4], 16)
    blue = int(colour[4:], 16)
    c = 0
    while c < KB_COLS:
        r = 0
        while r < KB_ROWS:
            device.send_feature_report([0xCC, 0x01, get_led_id(r,c), red, green, blue, 0x6C])
            print(f'row {r} column {c}')
            r += 1
        c += 1

def set_keys_to_colour_rows(colour):
    """Set all keys on the keyboard to requested hex colour"""
    red = int(colour[:2], 16)
    green = int(colour[2:4], 16)
    blue = int(colour[4:], 16)
    r = 0
    while r < KB_ROWS:
        c = 0
        while c < KB_COLS:
            device.send_feature_report([0xCC, 0x01, get_led_id(r,c), red, green, blue, 0x6C])
            print(f'row {r} column {c}')
            c += 1
            time.sleep(1)
        r += 1

### Iterate over each effect to give me time to document them
i = 0
while i < 256:
    print(f'Magic Number: {i}')
    device.send_feature_report([0xCC, 0x00, i, 0x00, 0x00, 0x00, 0x00])
    i += 1
    time.sleep(2)

# Special animation
RAINBOW_EFFECT = [0xCC, 0x00, 0x09, 0x00, 0x00, 0x00, 0x00]