
# Clevo Per-Key Keyboard Backlighting

Certain Clevo (Metabox/Tuxedo) laptops are equipped with a keyboard that supports per-key RGB lighting. The device is attached to USB with ID 048D:8910 (ITE IT829x). The backlight can be configured by sending a 7-byte HID feature report.

| Byte index |  Description / Value  |
| :--------: | --------------------- |
|   `0x00`   | `0xCC`                |
|   `0x01`   | Command ID            |
|   `0x02`   | Command Data          |
|   `0x03`   | Command Data          |
|   `0x04`   | Command Data          |
|   `0x05`   | Command Data          |
|   `0x06`   | Checksum / Commit (?) |
: Packet Structure

## Key Addressing

Keys on a standard Clevo keyboard may be envisioned as a 6x20 grid and addressed using the following functions:

```C
#define get_led_id(row, col)    (u8)( ((row & 0x07) << 5) | (col & 0x1f) )
```

```python
def get_led_id(row, col):
    return (row & 0x07) << 5 | (col & 0x1f)
```
There are actually 102 keys, with 114 LEDs.

The following "Special Keys" each have two LEDs assigned on the grid:

- Tab
- Caps Lock
- Shift
- Ctrl
- Enter
- Backspace
- Numpad Plus
- Numpad Enter

The Space key has 4 LEDs, across 5 addresses; (5,5) to (5,9) with (5,7) being a dead address.

(1,12), (2,15), (3,13), (4,1), (5,7) are dead addresses.

### Example Values (QWERTY)

|     Key      | Index  |    Binary    |  Hex   | Dec |
| ------------ | ------ | ------------ | ------ | --- |
| Esc          | (0,0)  | `0x00000000` | `0x0`  | 0   |
| Caps Lock    | (3,0)  | `0x01100000` | `0xC0` | 96  |
| G            | (3,6)  | `0x01100110` | `0x66` | 102 |
| Numpad Enter | (5,19) | `0x10110011` | `0xB3` | 179 |

## Commands

### `0x01`: Set Key Color

[`0xCC`, `0x01`, <KeyID>, <Red>, <Green>, <Blue>, <unknown, sometimes `0x00` sometimes 0x6C>]

| Byte index | Description |      Value       |
| :--------: | ----------- | ---------------- |
|   `0x00`   | Header (?)  | `0xCC`           |
|   `0x01`   | Command ID  | `0x01`           |
|   `0x02`   | KeyID       | `0x00` - `0xA3`  |
|   `0x03`   | Red         | `0x00` - `0xFF`  |
|   `0x04`   | Green       | `0x00` - `0xFF`  |
|   `0x05`   | Blue        | `0x00` - `0xFF`  |
|   `0x06`   | ???         | `0x00` or `0x6C` |

The purpose of the last byte is unknown; It may be some kind of CRC or an 'Apply' flag.

### `0x09`: Set Brightness and Speed

[`0xCC`, `0x01`, <Brightness>, <Speed>, `0x00`, `0x00`, <unknown, sometimes `0x00` sometimes `0x6C`>]

"Official" Brightness steps: 00, 02, 04, 06, 0A

| Byte index | Description |           Value            |
| :--------: | ----------- | -------------------------- |
|   `0x00`   | Header (?)  | `0xCC`                     |
|   `0x01`   | Command ID  | `0x09`                     |
|   `0x02`   | Brightness  | `0x00` - `0x0A`            |
|   `0x03`   | Speed       | `0x00` - `0x02`            |
|   `0x04`   |             | `0x00`                     |
|   `0x05`   |             | `0x00`                     |
|   `0x06`   |             | `0x00` or `0x0A` or `0x7F` |

The purpose of the last byte is unknown; It may be some kind of CRC or an 'Apply' flag.

### `0x00` Effects

[`0xCC`, `0x00`, <effect>, `0x00`, `0x00`, `0x00`, `0x00`]

List of effects:

|  Hex   |           Effect            |
| :----: | --------------------------- |
| `0x01` | Breathe                     |
| `0x02` | Colour Change               |
| `0x03` | Freeze current effect       |
| `0x04` | Colour Wave (From top left) |
| `0x06` | Blank Keyboard LEDs         |
| `0x07` | Blank Keyboard LEDs         |
| `0x09` | Random keys breathe         |
| `0x0A` | Scan                        |
| `0x0B` | Snake                       |
| `0x0C` | Blank Keyboard LEDs         |
| `0x13` | CLEVO flashing on keys      |

It does not appear to be possible to use any of the remaining bytes to configure the effect.

Some effects are under different Command IDs than `0x00`; [`0xCC`, `0x07`, ...] is supposed to be a 'Ripple' effect, however this appears to be unimplemented.

### Effects with additional control

#### `0x15` Wave

| Byte index | Description |           Value            |
| :--------: | ----------- | -------------------------- |
|   `0x00`   | Header (?)  | `0xCC`                     |
|   `0x01`   | Wave        | `0x15`                     |
|   `0x02`   | Direction   | `0x00` - `0x08`            |
|   `0x03`   | Speed       | `0x00`                     |
|   `0x04`   |             | `0x00`                     |
|   `0x05`   |             | `0x00`                     |
|   `0x06`   |             | `0x00` or `0x0A` or `0x7F` |

Directions

| Value  | Wave From Direction |
| :----: | ------------------- |
| `0x00` | Bottom Right        |
| `0x01` | Bottom Right        |
| `0x02` | Bottom Left         |
| `0x03` | Top Right           |
| `0x04` | Top Left            |
| `0x05` | Bottom              |
| `0x06` | Top                 |
| `0x07` | Right               |
| `0x08` | Left                |

#### `0x16` Snake

| Byte index | Description |           Value            |
| :--------: | ----------- | -------------------------- |
|   `0x00`   | Header (?)  | `0xCC`                     |
|   `0x01`   | Snake       | `0x16`                     |
|   `0x02`   | Direction   | `0x00` - `0x08`            |
|   `0x03`   | Speed       | `0x00`                     |
|   `0x04`   |             | `0x00`                     |
|   `0x05`   |             | `0x00`                     |
|   `0x06`   |             | `0x00` or `0x0A` or `0x7F` |

Directions

| Value  | Snake From Direction |
| :----: | -------------------- |
| `0x00` | Top Left             |
| `0x01` | Top Left             |
| `0x02` | Top Right            |
| `0x03` | Bottom Left          |
| `0x04` | Bottom Right         |
| `0x05` | Not a Snake!         |

Values between 0x05 and 0x10 seem to be test modes or partially implemented animations.

### Effects not under `0x00` with no configuration

- Breathe: [`0xCC`, `0x0A`, `0x00`, `0x00`, `0x00`, `0x00`, `0x0A`]
- Blink: [`0xCC`, `0x0B`, `0x00`, `0x00`, `0x00`, `0x00`, `0x0A`]
- Light up a few specific keys: 0x80
- Light up whole row except first two LEDs: 0x81 or 0xE0 (Function Keys / Top Row) to 0x86 or 0xE5 (Space / Bottom Row)

### `0x07`, `0x08` Blank Keyboard

[`0xCC`, `0x08`, `0x00`, `0x00`, `0x00`, `0x00`, `0x0A`]

Blanks the keyboard. At this time it is unknown how to un-toggle the blank state.
It may be reset by setting key colour values or by sending a different effect.
# Sources

- https://gitlab.com/CalcProgrammer1/OpenRGB/-/wikis/Clevo-Per-Key-Keyboard
- Tuxedo-Keyboard-ITE source (gitlab://matt.jolly/tuxedo-keyboard-ite)
- https://github.com/matheusmoreira/ite-829x
- Way too much time spent sending different values to a chip and recording the results
